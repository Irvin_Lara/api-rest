const express = require('express'); // invocamos express

function plus(req, res, next){
  let x = parseInt(req.params.n1);
  let y = parseInt(req.params.n2);
  let z = x + y;
  res.send(`${z}`);
};

function multiply(req, res, next){
  let x = parseInt(req.body.n1);
  let y = parseInt(req.body.n2);
  let z = x * y;
  res.send(`${z}`);
};

function divide(req, res, next){
  let x = parseInt(req.params.n1);
  let y = parseInt(req.params.n2);
  let z = x / y;
  res.send(`${z}`);
};

function minus(req, res, next){
  let x = parseInt(req.params.n1);
  let y = parseInt(req.params.n2);
  let z = x - y;
  res.send(`${z}`);
};

// Para que el archivo de rutas pueda usar esas funciones sin problema
module.exports = {
  plus,
  minus,
  multiply,
  divide
};
